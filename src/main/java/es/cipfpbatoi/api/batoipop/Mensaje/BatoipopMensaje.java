package es.cipfpbatoi.api.batoipop.Mensaje;
// Generated 15 feb 2022 17:20:32 by Hibernate Tools 5.4.32.Final


import javax.persistence.*;

@Entity
@Table(name = "batoipop_mensaje")
public class BatoipopMensaje implements java.io.Serializable {

	@Id
	@GeneratedValue(
			strategy = GenerationType.IDENTITY
	)
	private int id;
	private Integer usuario_receptor;
	private Integer usuario_emisor;
	private String texto;

	public BatoipopMensaje() {
	}

	public BatoipopMensaje(int id) {
		this.id = id;
	}

	public BatoipopMensaje(int id, Integer integerReceptor,
						   Integer usuario_emisor, String texto) {
		this.id = id;
		this.usuario_receptor = integerReceptor;
		this.usuario_emisor = usuario_emisor;
		this.texto = texto;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getUsuarioReceptor() {
		return this.usuario_receptor;
	}

	public void setUsuarioReceptor(Integer usuario_receptor) {
		this.usuario_receptor = usuario_receptor;
	}

	public Integer getUsuarioEmisor() {
		return this.usuario_emisor;
	}

	public void setUsuarioEmisor(Integer usuario_emisor) {
		this.usuario_emisor = usuario_emisor;
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
}
