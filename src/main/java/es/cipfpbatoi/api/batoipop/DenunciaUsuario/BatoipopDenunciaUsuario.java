package es.cipfpbatoi.api.batoipop.DenunciaUsuario;
// Generated 15 feb 2022 17:20:32 by Hibernate Tools 5.4.32.Final

import javax.persistence.*;

@Entity
@Table(name = "batoipop_denunciausuario")
public class BatoipopDenunciaUsuario implements java.io.Serializable {
	@Id
	@GeneratedValue(
			strategy = GenerationType.IDENTITY
	)
	private Integer id;
	private Integer denunciado;
	private Integer denunciante;

	public BatoipopDenunciaUsuario() {
	}

	public BatoipopDenunciaUsuario(Integer id, Integer denunciado, Integer denunciante) {
		this.id = id;
		this.denunciado = denunciado;
		this.denunciante = denunciante;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDenunciado() {
		return denunciado;
	}

	public void setDenunciado(Integer denunciado) {
		this.denunciado = denunciado;
	}

	public Integer getDenunciante() {
		return denunciante;
	}

	public void setDenunciante(Integer denunciante) {
		this.denunciante = denunciante;
	}
}
