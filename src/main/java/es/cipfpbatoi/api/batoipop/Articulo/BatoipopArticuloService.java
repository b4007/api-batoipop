package es.cipfpbatoi.api.batoipop.Articulo;

import es.cipfpbatoi.api.batoipop.Empleado.BatoipopEmpleado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BatoipopArticuloService {
    @Autowired
    private BatoipopArticuloRepository batoipopArticuloRepository;

    public BatoipopArticuloService() {
    }

    public List<BatoipopArticulo> listAllArticulos() {
        return this.batoipopArticuloRepository.findAll();
    }

    public BatoipopArticulo getArticuloById(Integer id) {
        return this.batoipopArticuloRepository.findById(id).get();
    }

    public BatoipopArticulo saveArticulo(BatoipopArticulo art) {
        return this.batoipopArticuloRepository.save(art);
    }

    public void deleteArticulo(Integer id) {
        this.batoipopArticuloRepository.deleteById(id);
    }
}
