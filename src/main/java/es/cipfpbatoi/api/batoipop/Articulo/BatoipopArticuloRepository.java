package es.cipfpbatoi.api.batoipop.Articulo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BatoipopArticuloRepository extends JpaRepository<BatoipopArticulo, Integer> {
}
