package es.cipfpbatoi.api.batoipop.Articulo;

import es.cipfpbatoi.api.batoipop.Empleado.BatoipopEmpleado;
import es.cipfpbatoi.api.batoipop.Empleado.BatoipopEmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping({"/articulo"})
public class BatoipopArticuloController {
    @Autowired
    BatoipopArticuloService batoipopArticuloService;

    public BatoipopArticuloController() {
    }

    @GetMapping({""})
    public List<BatoipopArticulo> list() {
        return this.batoipopArticuloService.listAllArticulos();
    }

    @GetMapping({"/{id}"})
    public ResponseEntity<BatoipopArticulo> get(@PathVariable Integer id) {
        try {
            BatoipopArticulo art = this.batoipopArticuloService.getArticuloById(id);
            return new ResponseEntity(art, HttpStatus.OK);
        } catch (NoSuchElementException var3) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopArticulo art) {
        BatoipopArticulo artNuevo = batoipopArticuloService.saveArticulo(art);
        return new ResponseEntity<>(artNuevo.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopArticulo art, @PathVariable Integer id) {
        try {
            BatoipopArticulo artActual = batoipopArticuloService.getArticuloById(id);
            artActual.setBatoipopCategoria(art.getBatoipopCategoria());
            artActual.setBatoipopUsuarioByComprador(art.getBatoipopUsuarioByComprador());
            artActual.setBatoipopUsuarioByVendedor(art.getBatoipopUsuarioByVendedor());
            artActual.setNombre(art.getNombre());
            artActual.setPrecio(art.getPrecio());
            artActual.setAntigüedad(art.getAntigüedad());
            artActual.setDescripcion(art.getDescripcion());
            batoipopArticuloService.saveArticulo(artActual);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            batoipopArticuloService.deleteArticulo(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
