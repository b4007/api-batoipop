package es.cipfpbatoi.api.batoipop.Categoria;
// Generated 15 feb 2022 17:20:32 by Hibernate Tools 5.4.32.Final

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import es.cipfpbatoi.api.batoipop.Articulo.*;

import javax.persistence.*;

@Entity
@Table(name = "batoipop_categoria")
public class BatoipopCategoria {
	@Id
	@GeneratedValue(
			strategy = GenerationType.IDENTITY
	)
	private Integer id;
	private String nombre;
	private String descripcion;

	public BatoipopCategoria(Integer id, String nombre, String descripcion) {
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public BatoipopCategoria() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
