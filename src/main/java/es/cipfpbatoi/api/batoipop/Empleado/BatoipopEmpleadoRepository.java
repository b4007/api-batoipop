package es.cipfpbatoi.api.batoipop.Empleado;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BatoipopEmpleadoRepository extends JpaRepository<BatoipopEmpleado, Integer> {
}
