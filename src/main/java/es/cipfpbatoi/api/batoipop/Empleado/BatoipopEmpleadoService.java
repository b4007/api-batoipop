//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package es.cipfpbatoi.api.batoipop.Empleado;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BatoipopEmpleadoService {
    @Autowired
    private BatoipopEmpleadoRepository batoipopEmpleadoRepository;

    public BatoipopEmpleadoService() {
    }

    public List<BatoipopEmpleado> listAllEmpleados() {
        return this.batoipopEmpleadoRepository.findAll();
    }

    public BatoipopEmpleado getEmpleadoById(Integer id) {
        return this.batoipopEmpleadoRepository.findById(id).get();
    }

    public BatoipopEmpleado saveEmpleado(BatoipopEmpleado emp) {
        return this.batoipopEmpleadoRepository.save(emp);
    }

    public void deleteEmpleado(Integer id) {
        this.batoipopEmpleadoRepository.deleteById(id);
    }
}
