//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package es.cipfpbatoi.api.batoipop.Empleado;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/empleado"})
public class BatoipopEmpleadoController {
    @Autowired
    BatoipopEmpleadoService batoipopEmpleadoService;

    public BatoipopEmpleadoController() {
    }

    @GetMapping({""})
    public List<BatoipopEmpleado> list() {
        return this.batoipopEmpleadoService.listAllEmpleados();
    }

    @GetMapping({"/{id}"})
    public ResponseEntity<BatoipopEmpleado> get(@PathVariable Integer id) {
        try {
            BatoipopEmpleado emp = this.batoipopEmpleadoService.getEmpleadoById(id);
            return new ResponseEntity(emp, HttpStatus.OK);
        } catch (NoSuchElementException var3) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopEmpleado emp) {
        BatoipopEmpleado empNuevo = batoipopEmpleadoService.saveEmpleado(emp);
        return new ResponseEntity<>(empNuevo.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopEmpleado emp, @PathVariable Integer id) {
        try {
            BatoipopEmpleado empActual = batoipopEmpleadoService.getEmpleadoById(id);
            empActual.setNombre(emp.getNombre());
            empActual.setApellidos(emp.getApellidos());
            empActual.setEmail(emp.getEmail());
            empActual.setContraseña(emp.getContraseña());
            batoipopEmpleadoService.saveEmpleado(empActual);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            batoipopEmpleadoService.deleteEmpleado(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
