package es.cipfpbatoi.api.batoipop.EtiquetaArticulo;
// Generated 15 feb 2022 17:20:32 by Hibernate Tools 5.4.32.Final

import es.cipfpbatoi.api.batoipop.Etiqueta.BatoipopEtiqueta;

import javax.persistence.*;

@Entity
@Table(name = "batoipop_etiqueta_articulo")
public class BatoipopEtiquetaArticulo implements java.io.Serializable {

	@Id @GeneratedValue(
			strategy = GenerationType.IDENTITY
	)
	private Integer etiquetaid;
	private Integer articuloid;

	public BatoipopEtiquetaArticulo() {
	}

	public BatoipopEtiquetaArticulo(Integer etiquetaid, Integer articuloid) {
		this.etiquetaid = etiquetaid;
		this.articuloid = articuloid;
	}

	public Integer getEtiquetaid() {
		return etiquetaid;
	}

	public void setEtiquetaid(Integer etiquetaid) {
		this.etiquetaid = etiquetaid;
	}

	public Integer getArticuloid() {
		return articuloid;
	}

	public void setArticuloid(Integer articuloid) {
		this.articuloid = articuloid;
	}
}
