package es.cipfpbatoi.api.batoipop.Etiqueta;
// Generated 15 feb 2022 17:20:32 by Hibernate Tools 5.4.32.Final

import es.cipfpbatoi.api.batoipop.Articulo.BatoipopArticulo;
import es.cipfpbatoi.api.batoipop.EtiquetaArticulo.BatoipopEtiquetaArticulo;
import javax.persistence.Id;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "batoipop_etiqueta")
public class BatoipopEtiqueta implements java.io.Serializable {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private int id;
    private String nombre;

    public BatoipopEtiqueta() {
    }

    public BatoipopEtiqueta(int id) {
        this.id = id;
    }

    public BatoipopEtiqueta(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
