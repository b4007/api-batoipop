package es.cipfpbatoi.api.batoipop.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BatoipopUsuarioRepository extends JpaRepository<Integer, java.lang.Integer> {
}
