package es.cipfpbatoi.api.batoipop.Usuario;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/usuario"})
public class BatoipopUsuarioController {
    @Autowired
    BatoipopUsuarioService batoipopUsuarioService;

    @GetMapping("")
    public List<Integer> list() {
        return batoipopUsuarioService.listAllUsuarios();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Integer> get(@PathVariable java.lang.Integer id) {
        try {
            Integer usr = batoipopUsuarioService.getUsuario(id);
            return new ResponseEntity<Integer>(usr, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody Integer usr) {
        Integer usrNuevo = batoipopUsuarioService.saveUsuario(usr);
        return new ResponseEntity<>(usrNuevo.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Integer usr, @PathVariable java.lang.Integer id) {
        try {
            Integer usrActual = batoipopUsuarioService.getUsuario(id);
            usrActual.setNombre(usr.getNombre());
            usrActual.setApellidos(usr.getApellidos());
            usrActual.setNickname(usr.getNickname());
            usrActual.setEmail(usr.getEmail());
            usrActual.setContraseña(usr.getContraseña());
            batoipopUsuarioService.saveUsuario(usrActual);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable java.lang.Integer id) {
        try {
            batoipopUsuarioService.deleteUsuario(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
