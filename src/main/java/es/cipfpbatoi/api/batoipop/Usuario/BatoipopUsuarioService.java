package es.cipfpbatoi.api.batoipop.Usuario;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Transactional
public class BatoipopUsuarioService {
    @Autowired
    private BatoipopUsuarioRepository batoipopUsuarioRepository;

    public List<Integer> listAllUsuarios() {
        return batoipopUsuarioRepository.findAll();
    }

    public Integer getUsuario(java.lang.Integer id) {
        return batoipopUsuarioRepository.findById(id).get();
    }

    public Integer saveUsuario(Integer usr) {
        return batoipopUsuarioRepository.save(usr);
    }

    public void deleteUsuario(java.lang.Integer id) {
        batoipopUsuarioRepository.deleteById(id);
    }
}
